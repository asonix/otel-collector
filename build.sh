#!/usr/bin/env bash

set -xe

export PATH=$PATH:$HOME/go/bin:/usr/local/go/bin

rm -rf ./artifacts

export GOARCH=arm64
opentelemetry-collector-builder --output-path=./artifacts --config=./.otelcol-builder.yaml

sudo docker build \
  --pull \
  -t asonix/otel-collector:v0.35.0 \
  -f Dockerfile \
  .

sudo docker tag asonix/otel-collector:v0.35.0 asonix/otel-collector:latest

sudo docker push asonix/otel-collector:v0.35.0
sudo docker push asonix/otel-collector:latest
