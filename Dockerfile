ARG REPO_ARCH=arm64v8

FROM asonix/rust-runner:$REPO_ARCH-latest

COPY ./artifacts/otelcol-custom /usr/local/bin/opentelemetry-collector
COPY ./otelcol.yaml /opt/app/otelcol.yaml

EXPOSE 4317
ENTRYPOINT ["/sbin/tini", "--"]
CMD /usr/local/bin/opentelemetry-collector --config=/opt/app/otelcol.yaml
